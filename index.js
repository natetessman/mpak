const fs = require( 'fs' );
const p = require( 'path' );
const babel = require( 'babel-core' );

module.exports = function ( entry, options ) {
	options = Object.assign( {
		async: true,
		silent: false,
		alias: [
			'document',
			'window',
			'setTimeout',
			'clearTimeout'
		],
		basePath: '',
		libraries: {},
		transformAlias: true,
		transformGlobal: true,
		transformConstants: true,
		stripAssert: true,
		replaceBoolean: true,
		globalIdentifier: 'global',
		constantIdentifier: 'C',
	}, options );

	if( !options.alias ) options.alias = [];

	if( options.transformGlobal && !options.globalIdentifier ) throw new Error( `transform global enabled but no global identifier set` );

	if( !Array.isArray( options.globalIdentifier ) ) {
		options.globalIdentifier = [ options.globalIdentifier ];
	}

	if( options.transformAlias && options.alias.includes( 'require' ) ) throw new Error( `can't alias require` );
	if( options.transformAlias && options.globalIdentifier.some( v => options.alias.includes( v ) ) )  throw new Error( `can't alias any of the global identifiers: ${options.globalIdentifier}` );
	if( options.transformAlias && options.constantIdentifier.some( v => options.alias.includes( v ) ) )  throw new Error( `can't alias any of the constant identifiers: ${options.constantIdentifier}` );
	if( options.transformAlias && options.alias.includes( 'this' ) )    throw new Error( `can't alias this` );

	if( !entry ) throw new Error( 'No input file provided.' );
	// if( !options.bundleName ) options.bundleName = 'bundle.js';

	if( !options.basePath ) options.basePath = process.cwd();
	else options.basePath = p.resolve( options.basePath );

	var files = {};
	var moduleInjectionPoint = {};

	var ind = 0;

	var constInd = 0;

	var consts = {};

	function getConstName ( val ) {
		if( consts[ val ] ) return consts[ val ];
		var name = 'const_' + constInd;
		consts[ val ] = name;
		constInd += 1;

		return name;
	}

	function processFile ( moduleName, sourceName, sourceDirectory, libraryPath ) {
		if( files[ moduleName ] ) {
			return files[ moduleName ];
		}
		
		var identifier = `_${ind}`;

		if( options.debug ) {
			var rel = p.relative( options.basePath, moduleName );

			var path = p.basename( rel, p.extname( moduleName ) );
			var dir = p.dirname( rel );
			if( dir != '.' ) {
				path = dir + '/' + path;
			}
			identifier = `_${ind}_${ path.replace( /[^0-9a-zA-Z_$]/g, '_' ) }`;
		}

		var file = {
			libBase: libraryPath,
			moduleName,
			identifier,
			code: '',
			dependents: [],
			dependencies: [],
			globals: [],
			alias: [],
			constants: [],
			constDef: {},
		};
		ind += 1;

		files[ moduleName ] = file;

		babel.currentFile = file;
		var inCode;
		try {
			inCode = fs.readFileSync( moduleName );
		} catch( err ) {
			if( sourceName ) {
				throw new Error( `Could not find module '${sourceName}' in '${sourceDirectory}'. (Resolved to '${moduleName}')` );
			} else {
				throw new Error( `Entry file not found at '${moduleName}'.`);
			}
		}

		if( options.preprocessor ) {
			var out = options.preprocessor( inCode.toString(), moduleName );
			if( out && typeof out === 'string' ) {
				inCode = out;
			}
		}

		try {
			var out = babel.transform( inCode, {
				plugins: [mpak]
			});
		} catch( err ) {
			console.log( `Error in ${moduleName}` );
			throw err;
		}

		file.code = out.code;

		return file;
	}

	function mpak ( babel ) {
		var t = babel.types;

		return {
			pre: function ( state ) {
				this.currentFile = babel.currentFile;
			},
			visitor: {
				StringLiteral: function ( path ) {
				},
				Directive: function ( path ) {
					if( path.node.value.value === '@module_entry' ) {
						if( moduleInjectionPoint.file ) throw new Error( "can't have multiple injection points!" );
						moduleInjectionPoint.file = this.currentFile;
						moduleInjectionPoint.loc = path.node.loc.start.line;
					}
				},
				Identifier: function ( path ) {
					var node = path.node;
					var name = node.name;

					if( name === 'exports' ) {
						if( !path.scope.hasBinding( name ) ) {
							this.currentFile.usesModule = true;
							path.replaceWith( t.identifier( this.currentFile.identifier ) );
						}
					}

					if( options.transformAlias ) {
						var isAlias = options.alias.includes( name );

						if( isAlias ) {
							if( path.key !== 'property' || path.parentPath.node.computed ) {
								if( name === 'indexOf' ) console.log( path.node.name, path.key, path.node.loc.start.line, this.currentFile.moduleName );
								this.currentFile.alias.push( name );
								node.name = 'alias_' + name;
							} else {
								// if( name === 'indexOf' ) console.log( path.node.name, path.key, path.node.loc.start.line, this.currentFile.moduleName );
								if( path.parentPath.key === 'callee' ) {
									var callExpr = path.parentPath.parentPath.node;
									var obj = path.parentPath.node.object;
									// console.log( path.node.name, path.key, path.node.loc.start.line, this.currentFile.moduleName );

									var argCount = callExpr.arguments.length;
									this.currentFile.alias.push( [ name, argCount ] );
									node.name = 'alias_func_' + name;

									callExpr.arguments.unshift( obj );

									path.parentPath.replaceWith( node );
								}
							}
						}
					}

					if( options.stripAssert ) {
						if( name === 'assert' ) {
							var p = path;

							while( p && p.type !== 'ExpressionStatement' ) {
								if( p.key === 'label' || ( p.key === 'property' && !p.parentPath.node.computed ) ) {
									p = null;
									break;
								}
								p = p.parentPath;
							}

							if( p ) p.remove();
						}
					}
				},
				Literal: function ( path ) {
					if( options.replaceBoolean ) {
						if( path.node.type === 'BooleanLiteral' ) {
							path.replaceWith( t.numericLiteral( path.node.value ? 1 : 0 ) );
						} else if( path.node.type === 'NullLiteral' ) {
							// path.replaceWith( t.numericLiteral( 0 ) );
						} else if( path.node.type === 'StringLiteral' ) {
							if( path.node.value === '@module_entry' ) {
								if( moduleInjectionPoint.file ) throw new Error( "can't have multiple injection points!" );

								moduleInjectionPoint.file = this.currentFile;
								moduleInjectionPoint.loc = path.node.loc.start.line;
							}
						}
					}
				},
				MemberExpression: function ( path ) {
					var node = path.node;
					var isModule = node.property.name === 'exports' && node.object.name === 'module' && !node.computed;
					var globalIndex = options.globalIdentifier.indexOf( node.object.name );
					var constIndex  = options.constantIdentifier.indexOf( node.object.name );

					if( isModule ) {
						this.currentFile.usesModule = true;
						path.replaceWith( t.identifier( this.currentFile.identifier ) );
					}

					if( options.transformGlobal ) {
						if( globalIndex > -1 ) {
							var name = `${options.globalIdentifier[ globalIndex ]}_${node.property.name}`;
							this.currentFile.globals.push( name );
							path.replaceWith( t.identifier( name ) );
						}
					}

					if( options.transformConstants ) {
						if( constIndex > -1 ) {
							var name = `${options.constantIdentifier[ constIndex ]}_${node.property.name}`;


							if( path.parentPath.type === 'AssignmentExpression' && path.key === 'left' ) {
								if( this.currentFile.constDef[ name ] > -1 ) {
									throw new Error( `can't redefine global constant '${ node.property.name }' (${ this.currentFile.moduleName }:${ node.loc.start.line }:${ node.loc.start.column })` );
								}

								path.parentPath.parentPath.replaceWith( t.variableDeclaration( 'var', [ t.variableDeclarator( t.identifier( name ), path.parentPath.node.right ) ] ) );

								this.currentFile.constDef[ name ] = 1;
							} else {
								this.currentFile.constants.push( name );
								path.replaceWith( t.identifier( name ) );
							}

						}
					}
				},
				NewExpression: function ( path ) {
					var callee = path.node.callee;
				},
				AssignmentExpression: function ( path ) {
					var left = path.node.left;
					if( left.type !== "MemberExpression" ) return;

					var isModule = left.property.name == 'exports' && left.object.name == 'module' && !left.computed;

					if( isModule ) {
						if( this.currentFile.hasModuleDeclaration ) {
							console.log( `module redefinition (${ this.currentFile.moduleName }:${ left.loc.start.line }:${ left.loc.start.column })` );

						}
						this.currentFile.hasModuleDeclaration = true;

						path.parentPath.replaceWith( t.variableDeclaration( 'var', [ t.variableDeclarator( t.identifier( this.currentFile.identifier ), path.node.right ) ] ) );
					}
				},
				CallExpression: function ( path ) {
					var func = path.node.callee;

					if( !func.name ) return;

					if( func.name !== 'require' ) return;

					var args = path.node.arguments;

					var arg0 = args[ 0 ];

					if( args.length != 1 ) {
						throw new Error( 'require must have only one argument.' );
					}

					arg0.isRequire = true;

					var original = arg0.value;
					var filename = arg0.value;


					var cwd = p.dirname( this.currentFile.moduleName );
					let libBase = this.currentFile.libBase;

					if( filename[ 0 ] == '/' ) {
						// make filename root relative to cwd
						filename = p.join( libBase || options.basePath, filename.substring( 1 ) );
					} else if( filename[ 0 ] == '@' ) {
						// make filename root relative to library path

						var libName = filename.substring( 1, filename.indexOf( '/' ) );
						var libPath = options.libraries[ libName ];
						if( !libPath ) {
							throw new Error( `Unknown library directive ${libName} specified.` );
						}
						filename = p.join( libPath, filename.substring( filename.indexOf( '/' ) ) );

						cwd = libPath;
						libBase = libPath;
					}

					var up = processFile( p.resolve( cwd, filename + '.js' ), original, cwd, libBase );
					up.dependents.push( this.currentFile.identifier );
					this.currentFile.dependencies.push( up.identifier );

					if( path.parentPath.node.type == 'VariableDeclarator' ) {
						if( !up.usesModule ) {
							throw new Error( `Trying to set a variable to the return value of a module that does not have any exports! \n\t ${path.parentPath.node.loc.start.line} | ${path.parentPath.node.id.name} = require('${arg0.value}').` );
						}

						if( !options.debug ) {
							path.parentPath.scope.rename( path.parentPath.node.id.name, up.identifier );
							path.parentPath.remove();
							return;
						}
					}


					if( path.parentPath.node.type === 'ExpressionStatement' ) { // case: require( 'module' );
						path.remove();
					} else { // case: var blah = require( 'module' );
						path.replaceWith( 
							t.identifier( up.identifier )
						);
					}
				}
			},
			post: function ( state ) {
				// console.log( state );
			}
		};
	}

	if( !options.silent ) {
		if( options.bundleName ) {
			console.log( `Building bundle '${options.bundleName}'...` );
		} else {
			console.log( `Building bundle...` );
		}
	}

	var startTime = Date.now();

	var baseFile = processFile( p.resolve( entry ) );

	var baseBundle = '"@module_entry";';

	var bundle = '';

	var globals = [];
	var alias = [];
	var constants = [];
	var constDef = {};

	var emittedFiles = {};

	if( moduleInjectionPoint.file ) {
		if( moduleInjectionPoint.file.dependents.length !== 0 ) {
			throw new Error( 
				`The file with the injection point cannot have any dependents! Has ${moduleInjectionPoint.file.dependents.length}: ${moduleInjectionPoint.file.dependents}` 
			);
		}

		baseBundle = moduleInjectionPoint.file.code;

		delete files[ moduleInjectionPoint.file.moduleName ];
	}

	var fileArr = Object.keys( files ).map( key => files[ key ] );

	var fileCount = 0;

	while( fileArr.length > 0 ) {
		for (var i = 0; i < fileArr.length; i++) {
			var file = fileArr[ i ];

			var shouldEmit = true;
			for (var j = 0; j < file.dependencies.length; j++) {
				// console.log( file.dependencies[ j ], emittedFiles[ file.dependencies[ j ] ], emittedFiles );
				if( !emittedFiles[ file.dependencies[ j ] ] ) shouldEmit = false;
			}

			if( shouldEmit ) {
				fileCount += 1;

				globals.push( ...file.globals );
				alias.push( ...file.alias );
				constants.push( ...file.constants );

				for( var name in file.constDef ) {
					constDef[ name ] = 1;
				}

				if( options.debug ) bundle += `/// begin: ${ file.moduleName }\n`;

				if( file.usesModule ) {
					if( !file.hasModuleDeclaration ) {
						bundle += `var ${file.identifier} = {};\n`;
					}
				}

				bundle += file.code + '\n';

				if( options.debug ) bundle += `/// end: ${ file.moduleName }\n`;
				
				fileArr.splice( i--, 1 );
				emittedFiles[ file.identifier ] = 1;
			}
		}
	}

	if( options.transformGlobal ) {
		if( globals.length ) {
			globals = [...new Set( globals )];
			bundle = `var ${globals.join(',')};\n${bundle}`;
		}
	} else {
		let globalVars = [];

		for( let name of options.globalIdentifier ) {
			globalVars.push( `var ${name} = {};` );
		}
		
		bundle = `${globalVars.join( '\n' )}${bundle}`;
	}

	if( options.transformAlias ) {
		var mostArgs = {};
		for( var i = 0; i < alias.length; i++ ) {
			var a = alias[ i ];
			if( Array.isArray( a ) ) {
				if( !mostArgs[ a[ 0 ] ] || mostArgs[ a[ 0 ] ] < a[ 1 ] ){
					mostArgs[ a[ 0 ] ] = a[ 1 ];
				}

				alias.splice( i--, 1 );
			}
		}

		// @todo: only alias functions that are worth it in terms of savings:
		// I will need to switch to using the parser/generator manually in order to make this work properly
		// maybe switch to acorn? would allow me to also create new token types :v

		// cost logic:
		// function(1,2,3,4,5){return 1.indexOf(2,3,4,5)}
		// a=function(1){1.()}
		// save per call = func_length - 2
		// cost = 19 + func_length + argCount * 4 - 1 + 7 * hasReturn
		// in order to be worth it, cost < ( func_length - 2 ) * callCount

		alias = [...new Set( alias )];
		alias = alias.map( a => `alias_${a} = ${a}` );

		for( var a in mostArgs ) {
			var args = [];
			for( var i = 0; i < mostArgs[ a ]; i++ ) args.push( `arg${i}` );
			args = args.join( ', ' );
			var callingArgs = args;
			if( callingArgs ) callingArgs = `, ${callingArgs}`;
			var str = `alias_func_${a} = function ( obj${callingArgs} ) { return obj.${a}( ${args} ) }`;
			alias.push( str );
		}

		if( alias.length ) {
			bundle = `var ${alias.join(',')};\n${bundle}`;
		}
	}

	if( options.transformConstants ) {
		constants = [...new Set( constants )];
		constants.forEach( function ( name ) {
			if( !constDef[ name ] ) {
				throw new Error( `use of undefined constant '${name}'` );
			}
		});
		// bundle = `var ${constants.join(',')};\n${bundle}`;
	}

	baseBundle = baseBundle.replace( /(?:'|")@module_entry(?:'|");/, bundle );

	if( options.bundleName ) {
		fs.writeFileSync( options.bundleName, baseBundle );
	}
	
	if( options.replaceBoolean ) {
		bundle = `var TRUE = !0, FALSE = !1;\n${bundle}`;
	}

	var elapsed = Date.now() - startTime;
	if( !options.silent ) console.log( `Success! ${fileCount} modules processed in ${ elapsed / 1000 } seconds.` );

	if( options.async ) {
		return new Promise ( ( res, rej ) => {
			res( baseBundle );
		});
	} else {
		return baseBundle;
	}
};
